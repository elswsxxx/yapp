<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
});

Route::get('/paso2', function () {
    return view('paso2');
});

Route::get('/paso3', function () {
    return view('paso3');
});

Route::get('/carro', function () {
    return view('carro');
});

Route::get('/sin-registro', function () {
    return view('sin-registro');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
