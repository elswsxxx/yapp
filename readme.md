## 1° crear archivo .env 
cp .env.example .env

## 2° configurar conexion base de datos mysql 
(por el momento no es necesario)

## 3° ejecutar instalador 
composer install

## 4° crear key 
php artisan key:generate

## 5° iniciar software
php artisan serve

## Rutas
'/' -> inicio

'/paso2' -> paso 2

'/paso3' -> paso 3

'/carro' -> vista de carro notificación que el producto se agregó al carro.

'/sin-registro' -> vista notificación que el usuario no se ha registrado.
