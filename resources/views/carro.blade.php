@extends('layouts.appweb')

@section('content')
    <!-- Intro -->
    <section id="intro" class="main style1 dark fullscreen">
        <div class="content">

            <!-- imagen central -->
            <div style="padding-top: 20px; padding-bottom: 20px;"><img src="images/asset4.svg" alt="" /></div>
            <!-- fin imagen central -->

            <!-- texto principal -->
            <p>¡Tu primer pedido se ha agregado al carro de compras!</p>
            <!-- fin texto principal -->

            <footer>
                <!-- botón -->
                <a href="#" class="btn yapp">Ir al carro de compras</a>
                <!-- fin botón -->
                <!-- botón -->
                <a href="#" class="btn purple">Seguir buscando productos</a>
                <!-- fin botón -->
            </footer>
        </div>
    </section>

@endsection


@section('css')
@endsection

@section('js')
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('script')
    <script>

    </script>
@endsection