@extends('layouts.appweb')

@section('content')
    <!-- Intro -->
    <section id="intro" class="main style1 dark fullscreen">
        <div class="content">
            <!-- texto header -->
            <header style="text-align: left;">
                <span style="font-size: 24px;">Inscríbete en tu tratamiento</span>
            </header>
            <!-- fin texto header -->

            <!-- imagen central -->
            <div style="padding-top: 20px; padding-bottom: 20px;"><img src="images/parox.png" alt="" /></div>
            <!-- fin imagen central -->

            <!-- texto principal -->
            <p class="titulo"><strong>MSD: Acompáñame</strong></p>
            <p class="texto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eros libero, pretium id consequat ac, malesuada condimentum ante. Nulla non leo ultricies, pretium mi quis, sodales ante. Curabitur ac justo porta massa aliquet ultricies. Maecenas blandit neque quis purus imperdiet iaculis. Donec eget sapien cursus, pretium diam nec, tempus ante. Praesent mollis quis orci ut gravida. Morbi tincidunt facilisis nulla, ac commodo metus ultricies et. </p>
            <!-- fin texto principal -->

            <footer>
                <!-- botón -->
                <a href="{{ url('/paso3') }}" class="btn yapp">Siguiente</a>
                <!-- fin botón -->
            </footer>
        </div>
    </section>

@endsection


@section('css')
@endsection

@section('js')
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('script')
    <script>

    </script>
@endsection