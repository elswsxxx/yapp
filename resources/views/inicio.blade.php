@extends('layouts.appweb')

@section('content')
    <!-- Intro -->
    <section id="intro" class="main style1 dark fullscreen">
        <div class="content">
            <!-- texto header -->
            <header style="text-align: left;">
                <span style="font-size: 24px;">Descubre los beneficios <strong>Yapp Life</strong></span>
            </header>
            <!-- fin texto header -->

            <!-- imagen central -->
            <div style="padding-top: 20px; padding-bottom: 20px;"><img src="images/asset1.svg" alt="" /></div>
            <!-- fin imagen central -->

            <!-- texto principal -->
            <p class="titulo"><strong>¿Qué es?</strong></p>
            <p class="texto">Yapp life es un club GRATUITO de beneficios que hemos creado para que nuestros usuarios sean premiados al seguir sus tratamientos.<br>
            Aquí encontrarás los mejores precios.</p>
            <!-- fin texto principal -->

            <footer>
                <!-- botón -->
                <a href="{{ url('/paso2') }}" class="btn yapp">Ver Tratamiento</a>
                <!-- fin botón -->
            </footer>
        </div>
    </section>

@endsection


@section('css')
@endsection

@section('js')
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('script')
    <script>

    </script>
@endsection