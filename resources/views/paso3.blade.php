@extends('layouts.appweb')

@section('content')
    <!-- Intro -->
    <section id="intro" class="main style1 dark fullscreen">
        <div class="content">
            <!-- texto header -->
            <header style="text-align: left;">
                <span style="font-size: 24px;">¿Cada cuántos días quieres recibir tu pedido?</span>
            </header>
            <!-- fin texto header -->

            <!-- imagen central -->
            <div style="padding-top: 20px; padding-bottom: 20px;"><img src="images/parox.png" alt="" /></div>
            <!-- fin imagen central -->

            <!-- formulario principal -->
            <div style="text-align: left;">
                <span>Elegir frecuencia de envío</span>
                <select class="form-control">
                    <option>30 días</option>
                </select>
                <div style="height: 20px;"></div>
                <span>Dirección de despacho</span>
                <input class="form-control" type="text" placeholder="(API Google)">
            </div>
            <!-- fin texto principal -->

            <footer>
                <!-- botón -->
                <a href="{{ url('/carro') }}" class="btn yapp">Siguiente</a>
                <!-- fin botón -->
            </footer>
        </div>
    </section>

@endsection


@section('css')
@endsection

@section('js')
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('script')
    <script>

    </script>
@endsection