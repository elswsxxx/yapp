@extends('layouts.appweb')

@section('content')
    <!-- Intro -->
    <section id="intro" class="main style1 dark fullscreen">
        <div class="content">
            <!-- texto header -->
            <header style="text-align: left;">
                <span style="font-size: 24px;">¡No tienes creado tu perfil!</span>
            </header>
            <!-- fin texto header -->


            <!-- texto principal -->
            <p class="texto">Para poder gestionar tus tratamientos y acceder a los mejores beneficios siempre, es necesario que completes algunos datos de perfil.</p>
            <!-- fin texto principal -->

            <footer>
                <!-- botón -->
                <a href="#" class="btn yapp">Crear perfil</a>
                <!-- fin botón -->
            </footer>
        </div>
    </section>

@endsection


@section('css')
@endsection

@section('js')
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('script')
    <script>

    </script>
@endsection